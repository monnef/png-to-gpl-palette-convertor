Simple CLI tool for creation of a GPL palette (GIMP palette format) from a PNG file.

One pixel = one color in palette, it does not handle duplicates.

Dependencies
============
* [Node](https://nodejs.org/en/)

Tested with Node 8.9.4 on Ubuntu 16.04 and verified generated palette file in Krita 3.3.2.

Usage
=====

```sh
npm install
npm start
node dist/main.js input.png output.gpl
```

where `input.png` is your existing PNG palette and `output.gpl` is the output file.

License
=======
GPLv3
