require! { fs }
PNG = require('pngjs').PNG
inputFileName = process.argv[2]
outputFileName = process.argv[3]

console.log('utility for converting PNG to GPL (GIMP palette format) by monnef')

if !inputFileName || !outputFileName
  console.error "Missing file name."
  process.exit 1

console.log "input = #{inputFileName}"
console.log "output = #{outputFileName}"

imgData = fs.readFileSync inputFileName
png = PNG.sync.read imgData

console.log "size = #{png.width} x #{png.height}"

res = "GIMP Palette\nName: Generated Palette\nColumns: 0\n#"
i = 0
for y from 0 til png.height
  for x from 0 til png.width
    idx = (png.width * y + x) .<<. 2
    r = png.data[idx]
    g = png.data[idx+1]
    b = png.data[idx+2]
    res += "\n#{r} #{g} #{b}"
    i++

console.log "read #{i} colors"

fs.writeFileSync(outputFileName, res)
console.log 'palette written'
